import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor( private api: ApiService) {
    this.counter = 1;
    this.getapi();
  }
 counter: number;
 datas: any;
  title = 'test';

  getapi() {
  this.api.getall().subscribe(res => {
    console.log(res['status']);
    this.datas = res['data'];
  });
  }
  ngOnInit(): void {
  }

  increment() {
    this.counter++;
    }

    decrement() {
      this.counter--;
      }
}
