import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestComponent } from './test.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

describe('TestComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  // tslint:disable-next-line:prefer-const
  let de: DebugElement;
  // tslint:disable-next-line:prefer-const
  let htmlElement: HTMLElement;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      declarations: [ TestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    de = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should increment the counter number by one', () => {
    const intial = component.counter;
    component.increment();
    const newvalue = component.counter;
    expect(newvalue).toBeGreaterThanOrEqual(intial);
 });
  it('should decrement the counter number by one', () => {
  const intial = component.counter;
  component.decrement();
  const newvalue = component.counter;
  expect(newvalue).toBeLessThanOrEqual(intial);
});

  it(`should have as title 'test'`, async(() => {
  fixture = TestBed.createComponent(TestComponent);
  component = fixture.debugElement.componentInstance;
  expect(component.title).toEqual('test');
}));

//   it('should have a p tag of `test works!`', () => {
//     expect(de.query(By.css('p')).nativeElement.innerText).toBe('test works!');
// });
  it('should display the number of the counter', () => {
expect(de.query(By.css('p')).nativeElement.innerText).toEqual('count:1');
  });
});



